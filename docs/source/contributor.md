# Contributors Guide

:::{tip}
Please fork this GitLab project and provide a merge request to contribute!
::: 

- The main implementation contains [perform_analysis](#main.perform_analysis).
- Please see the [main module](main) for further information.
